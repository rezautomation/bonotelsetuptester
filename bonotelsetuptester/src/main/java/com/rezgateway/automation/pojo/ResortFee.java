package com.rezgateway.automation.pojo;

public class ResortFee {
   
		private String From="";
		private String To="";
		private String RoomType="";
		private String Occupancy="";
		private String RatePlan="";
		private String ResortFee="";
		public String getFrom() {
			return From;
		}
		public void setFrom(String from) {
			From = from;
		}
		public String getTo() {
			return To;
		}
		public void setTo(String to) {
			To = to;
		}
		public String getRoomType() {
			return RoomType;
		}
		public void setRoomType(String roomType) {
			RoomType = roomType;
		}
		public String getOccupancy() {
			return Occupancy;
		}
		public void setOccupancy(String occupancy) {
			Occupancy = occupancy;
		}
		public String getRatePlan() {
			return RatePlan;
		}
		public void setRatePlan(String ratePlan) {
			RatePlan = ratePlan;
		}
		public String getResortFee() {
			return ResortFee;
		}
		public void setResortFee(String resortFee) {
			ResortFee = resortFee;
		}
		
}
