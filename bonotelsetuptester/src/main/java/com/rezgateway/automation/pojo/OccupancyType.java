package com.rezgateway.automation.pojo;

import java.util.ArrayList;



public class OccupancyType  {
		
		private String OccupancyName = ""; 
		private String StandardAdults= "";
		private String AdditionalAdults= "";
		private String Children="";
		private String RoomCombinationActive = "";
		private String RatePlanCombinationActive = "";
		private String TotalRooms="";
		private String MinimumNightsStay="";
		private String MaximumNightsStay="";
		private String Cutoff="";
		private ArrayList<RatePlan> rateplans = new ArrayList<RatePlan>();
		public String getOccupancyName() {
			return OccupancyName;
		}
		public void setOccupancyName(String occupancyName) {
			OccupancyName = occupancyName;
		}
		public String getStandardAdults() {
			return StandardAdults;
		}
		public void setStandardAdults(String standardAdults) {
			StandardAdults = standardAdults;
		}
		public String getAdditionalAdults() {
			return AdditionalAdults;
		}
		public void setAdditionalAdults(String additionalAdults) {
			AdditionalAdults = additionalAdults;
		}
		public String getChildren() {
			return Children;
		}
		public void setChildren(String children) {
			Children = children;
		}
		public String getRoomCombinationActive() {
			return RoomCombinationActive;
		}
		public void setRoomCombinationActive(String roomCombinationActive) {
			RoomCombinationActive = roomCombinationActive;
		}
		public String getRatePlanCombinationActive() {
			return RatePlanCombinationActive;
		}
		public void setRatePlanCombinationActive(String ratePlanCombinationActive) {
			RatePlanCombinationActive = ratePlanCombinationActive;
		}
		public String getTotalRooms() {
			return TotalRooms;
		}
		public void setTotalRooms(String totalRooms) {
			TotalRooms = totalRooms;
		}
		public String getMinimumNightsStay() {
			return MinimumNightsStay;
		}
		public void setMinimumNightsStay(String minimumNightsStay) {
			MinimumNightsStay = minimumNightsStay;
		}
		public String getMaximumNightsStay() {
			return MaximumNightsStay;
		}
		public void setMaximumNightsStay(String maximumNightsStay) {
			MaximumNightsStay = maximumNightsStay;
		}
		public String getCutoff() {
			return Cutoff;
		}
		public void setCutoff(String cutoff) {
			Cutoff = cutoff;
		}
		public ArrayList<RatePlan> getRateplans() {
			return rateplans;
		}
		public void setRateplans(ArrayList<RatePlan> rateplans) {
			this.rateplans = rateplans;
		}
		


}
