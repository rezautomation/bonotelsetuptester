package com.rezgateway.automation.pojo;


import com.rezgateway.automation.enums.CancellationBasedOnType;

public class HotelPolicies {
	
	private String PolicyType = "";
	private boolean ChildrenAllowed=false;
	private String ChargesFreeAge="";
	private String ChildRateApplicable="";
	private String CheckInTime="";
	private String CheckOutTime ="";
	private String HotelWeekdayClassification="";	
	private String HotelWeekendClassification="";
	private String AmendmentType="";
	private String CancellationModificationType="";
	private String RoomType ="";
	private String RatePlan ="";
	private String RateType ="";
	private String From ="";
	private String To ="";
	private String ArrivalDateLessThan="";
	private String CancellationBasedOn="";
	private String NoCancellationBasedOn="";
	private CancellationBasedOnType StandardCancellation = CancellationBasedOnType.No_Night;
	private CancellationBasedOnType NoShowCancellation = CancellationBasedOnType.Reservation_Value;
	
	
	
	
	public String getPolicyType() {
		return PolicyType;
	}
	public void setPolicyType(String policyType) {
		PolicyType = policyType;
	}
	public boolean isChildrenAllowed() {
		return ChildrenAllowed;
	}
	public void setChildrenAllowed(boolean childrenAllowed) {
		ChildrenAllowed = childrenAllowed;
	}
	public String getChargesFreeAge() {
		return ChargesFreeAge;
	}
	public void setChargesFreeAge(String chargesFreeAge) {
		ChargesFreeAge = chargesFreeAge;
	}
	public String getChildRateApplicable() {
		return ChildRateApplicable;
	}
	public void setChildRateApplicable(String childRateApplicable) {
		ChildRateApplicable = childRateApplicable;
	}
	public String getCheckInTime() {
		return CheckInTime;
	}
	public void setCheckInTime(String checkInTime) {
		CheckInTime = checkInTime;
	}
	public String getCheckOutTime() {
		return CheckOutTime;
	}
	public void setCheckOutTime(String checkOutTime) {
		CheckOutTime = checkOutTime;
	}
	public String getHotelWeekdayClassification() {
		return HotelWeekdayClassification;
	}
	public void setHotelWeekdayClassification(String hotelWeekdayClassification) {
		HotelWeekdayClassification = hotelWeekdayClassification;
	}
	public String getHotelWeekendClassification() {
		return HotelWeekendClassification;
	}
	public void setHotelWeekendClassification(String hotelWeekendClassification) {
		HotelWeekendClassification = hotelWeekendClassification;
	}
	public String getAmendmentType() {
		return AmendmentType;
	}
	public void setAmendmentType(String amendmentType) {
		AmendmentType = amendmentType;
	}
	public String getCancellationModificationType() {
		return CancellationModificationType;
	}
	public void setCancellationModificationType(String cancellationModificationType) {
		CancellationModificationType = cancellationModificationType;
	}
	public String getRoomType() {
		return RoomType;
	}
	public void setRoomType(String roomType) {
		RoomType = roomType;
	}
	public String getRatePlan() {
		return RatePlan;
	}
	public void setRatePlan(String ratePlan) {
		RatePlan = ratePlan;
	}
	public String getRateType() {
		return RateType;
	}
	public void setRateType(String rateType) {
		RateType = rateType;
	}
	public String getFrom() {
		return From;
	}
	public void setFrom(String from) {
		From = from;
	}
	public String getTo() {
		return To;
	}
	public void setTo(String to) {
		To = to;
	}
	public String getArrivalDateLessThan() {
		return ArrivalDateLessThan;
	}
	public void setArrivalDateLessThan(String arrivalDateLessThan) {
		ArrivalDateLessThan = arrivalDateLessThan;
	}
	public String getCancellationBasedOn() {
		return CancellationBasedOn;
	}
	public void setCancellationBasedOn(String cancellationBasedOn) {
		CancellationBasedOn = cancellationBasedOn;
	}
	public String getNoCancellationBasedOn() {
		return NoCancellationBasedOn;
	}
	public void setNoCancellationBasedOn(String noCancellationBasedOn) {
		NoCancellationBasedOn = noCancellationBasedOn;
	}
	public CancellationBasedOnType getStandardCancellation() {
		return StandardCancellation;
	}
	public void setStandardCancellation(CancellationBasedOnType standardCancellation) {
		StandardCancellation = standardCancellation;
	}
	public CancellationBasedOnType getNoShowCancellation() {
		return NoShowCancellation;
	}
	public void setNoShowCancellation(CancellationBasedOnType noShowCancellation) {
		NoShowCancellation = noShowCancellation;
	}
	
 	
}
