package com.rezgateway.automation.pojo;

import com.rezgateway.automation.enums.ContactType;

public class ContactDetails {
	
	private String ContactName = "";
	private String Email ="";
	private String Telephone ="";
	private String Fax = "";
	private String ContactMedia = "";
	private ContactType Type = ContactType.NONE;
	
	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return ContactName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return Email;
	}
	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return Telephone;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return Fax;
	}
	/**
	 * @return the contactMedia
	 */
	public String getContactMedia() {
		return ContactMedia;
	}
	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		ContactName = contactName;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		Email = email;
	}
	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		Fax = fax;
	}
	/**
	 * @param contactMedia the contactMedia to set
	 */
	public void setContactMedia(String contactMedia) {
		ContactMedia = contactMedia;
	}
	/**
	 * @param contactType the contactType to set
	 */

	public ContactType getType() {
		return Type;
	}
	public void setType(ContactType type) {
		Type = type;
	}
	
		
}
