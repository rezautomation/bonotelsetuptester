package com.rezgateway.automation.pojo;

public class RatePlan  {
 
	private String SetupRate="";
	private String Rateplan="";
	private String Amount="";
	private String AdjustOn="";
	
	private String FixInventoryBy ="";
	private String AlertNotification = "";
	
	private String NetRate="";
	private String AdditionalAdultRate="";
	private String ChildRate="";
	private String RackRate="";
	private String AdditionalAdultRackRate="";
	private String ChildRackRate="";
	
	
	
	public String getSetupRate() {
		return SetupRate;
	}
	public void setSetupRate(String setupRate) {
		SetupRate = setupRate;
	}
	public String getRateplan() {
		return Rateplan;
	}
	public void setRateplan(String rateplan) {
		Rateplan = rateplan;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getAdjustOn() {
		return AdjustOn;
	}
	public void setAdjustOn(String adjustOn) {
		AdjustOn = adjustOn;
	}
	public String getFixInventoryBy() {
		return FixInventoryBy;
	}
	public void setFixInventoryBy(String fixInventoryBy) {
		FixInventoryBy = fixInventoryBy;
	}
	public String getAlertNotification() {
		return AlertNotification;
	}
	public void setAlertNotification(String alertNotification) {
		AlertNotification = alertNotification;
	}
	public String getNetRate() {
		return NetRate;
	}
	public void setNetRate(String netRate) {
		NetRate = netRate;
	}
	public String getAdditionalAdultRate() {
		return AdditionalAdultRate;
	}
	public void setAdditionalAdultRate(String additionalAdultRate) {
		AdditionalAdultRate = additionalAdultRate;
	}
	public String getChildRate() {
		return ChildRate;
	}
	public void setChildRate(String childRate) {
		ChildRate = childRate;
	}
	public String getRackRate() {
		return RackRate;
	}
	public void setRackRate(String rackRate) {
		RackRate = rackRate;
	}
	public String getAdditionalAdultRackRate() {
		return AdditionalAdultRackRate;
	}
	public void setAdditionalAdultRackRate(String additionalAdultRackRate) {
		AdditionalAdultRackRate = additionalAdultRackRate;
	}
	public String getChildRackRate() {
		return ChildRackRate;
	}
	public void setChildRackRate(String childRackRate) {
		ChildRackRate = childRackRate;
	}
	public String getTaxes() {
		return Taxes;
	}
	public void setTaxes(String taxes) {
		Taxes = taxes;
	}
	public String getCost() {
		return Cost;
	}
	public void setCost(String cost) {
		Cost = cost;
	}
	public String getProfitMarkup() {
		return ProfitMarkup;
	}
	public void setProfitMarkup(String profitMarkup) {
		ProfitMarkup = profitMarkup;
	}
	public String getSellRate() {
		return SellRate;
	}
	public void setSellRate(String sellRate) {
		SellRate = sellRate;
	}
	private String Taxes="";
	private String Cost="";
	private String ProfitMarkup="";
	private String SellRate="";
	
	
}
