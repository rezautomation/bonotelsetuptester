package com.rezgateway.automation.pojo;

public class ProfitMarkup {
		
	private String SelectCustomerType="";
	private String RateRegion="";
	private String TourOperator="";
	private String ProfitMarkupType="";
	private String ProfitMarkupPeriodFrom="";
	private String ProfitMarkupPeriodTo="";
	private String TORateType="";
	private String Country="";
	private String City="";
	private String ApplyProfitMarkupTo	="";
	private String OverwriteSpecificMarkup="";
	private String SelectHotel="";
	private String RateType="";
	
	private String FixProfitMarkup ="";
	private String ProfitMarkup="";
	private String AdditionalAdultProfitMarkup ="";
	private String ChildProfitMarkup="";
	
	
	
	public String getSelectCustomerType() {
		return SelectCustomerType;
	}
	public void setSelectCustomerType(String selectCustomerType) {
		SelectCustomerType = selectCustomerType;
	}
	public String getRateRegion() {
		return RateRegion;
	}
	public void setRateRegion(String rateRegion) {
		RateRegion = rateRegion;
	}
	public String getTourOperator() {
		return TourOperator;
	}
	public void setTourOperator(String tourOperator) {
		TourOperator = tourOperator;
	}
	public String getProfitMarkupType() {
		return ProfitMarkupType;
	}
	public void setProfitMarkupType(String profitMarkupType) {
		ProfitMarkupType = profitMarkupType;
	}
	public String getProfitMarkupPeriodFrom() {
		return ProfitMarkupPeriodFrom;
	}
	public void setProfitMarkupPeriodFrom(String profitMarkupPeriodFrom) {
		ProfitMarkupPeriodFrom = profitMarkupPeriodFrom;
	}
	public String getProfitMarkupPeriodTo() {
		return ProfitMarkupPeriodTo;
	}
	public void setProfitMarkupPeriodTo(String profitMarkupPeriodTo) {
		ProfitMarkupPeriodTo = profitMarkupPeriodTo;
	}
	public String getTORateType() {
		return TORateType;
	}
	public void setTORateType(String tORateType) {
		TORateType = tORateType;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getApplyProfitMarkupTo() {
		return ApplyProfitMarkupTo;
	}
	public void setApplyProfitMarkupTo(String applyProfitMarkupTo) {
		ApplyProfitMarkupTo = applyProfitMarkupTo;
	}
	public String getOverwriteSpecificMarkup() {
		return OverwriteSpecificMarkup;
	}
	public void setOverwriteSpecificMarkup(String overwriteSpecificMarkup) {
		OverwriteSpecificMarkup = overwriteSpecificMarkup;
	}
	public String getSelectHotel() {
		return SelectHotel;
	}
	public void setSelectHotel(String selectHotel) {
		SelectHotel = selectHotel;
	}
	public String getRateType() {
		return RateType;
	}
	public void setRateType(String rateType) {
		RateType = rateType;
	}
	public String getFixProfitMarkup() {
		return FixProfitMarkup;
	}
	public void setFixProfitMarkup(String fixProfitMarkup) {
		FixProfitMarkup = fixProfitMarkup;
	}
	public String getProfitMarkup() {
		return ProfitMarkup;
	}
	public void setProfitMarkup(String profitMarkup) {
		ProfitMarkup = profitMarkup;
	}
	public String getAdditionalAdultProfitMarkup() {
		return AdditionalAdultProfitMarkup;
	}
	public void setAdditionalAdultProfitMarkup(String additionalAdultProfitMarkup) {
		AdditionalAdultProfitMarkup = additionalAdultProfitMarkup;
	}
	public String getChildProfitMarkup() {
		return ChildProfitMarkup;
	}
	public void setChildProfitMarkup(String childProfitMarkup) {
		ChildProfitMarkup = childProfitMarkup;
	}
	
	
}
