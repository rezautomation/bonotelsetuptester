package com.rezgateway.automation.pojo;

import java.util.ArrayList;

public class Room  {
	
	
	private String City = "";
	private String State = "";	
	private String Room_Type="";
	private String RoomType_ID="";
	private ArrayList<OccupancyType> occupancy=new ArrayList<OccupancyType>();
	private String FixInventoryBy ="";
	private String AlertNotification = "";
	private String TotalRooms="";
	private String MinimumNightsStay="";
	private String MaximumNightsStay="";
	private String Cutoff="";
	
		
	public String getCity() {
		return City;
	}
	public void setCity(String city) {
		City = city;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getRoom_Type() {
		return Room_Type;
	}
	public void setRoom_Type(String room_Type) {
		Room_Type = room_Type;
	}
	public String getRoomType_ID() {
		return RoomType_ID;
	}
	public void setRoomType_ID(String roomType_ID) {
		RoomType_ID = roomType_ID;
	}
	public ArrayList<OccupancyType> getOccupancy() {
		return occupancy;
	}
	public void setOccupancy(ArrayList<OccupancyType> occupancy) {
		this.occupancy = occupancy;
	}
	public String getFixInventoryBy() {
		return FixInventoryBy;
	}
	public void setFixInventoryBy(String fixInventoryBy) {
		FixInventoryBy = fixInventoryBy;
	}
	public String getAlertNotification() {
		return AlertNotification;
	}
	public void setAlertNotification(String alertNotification) {
		AlertNotification = alertNotification;
	}
	public String getTotalRooms() {
		return TotalRooms;
	}
	public void setTotalRooms(String totalRooms) {
		TotalRooms = totalRooms;
	}
	public String getMinimumNightsStay() {
		return MinimumNightsStay;
	}
	public void setMinimumNightsStay(String minimumNightsStay) {
		MinimumNightsStay = minimumNightsStay;
	}
	public String getMaximumNightsStay() {
		return MaximumNightsStay;
	}
	public void setMaximumNightsStay(String maximumNightsStay) {
		MaximumNightsStay = maximumNightsStay;
	}
	public String getCutoff() {
		return Cutoff;
	}
	public void setCutoff(String cutoff) {
		Cutoff = cutoff;
	}
	
	
}
	

	

