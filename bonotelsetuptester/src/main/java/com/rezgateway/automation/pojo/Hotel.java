package com.rezgateway.automation.pojo;


/**
 * @author Akila
 *
 */
import java.util.ArrayList;
import java.util.HashMap;

public class Hotel {

	//TODO: Add other Assignment, Rules, Amenities, Filters, Content ,Image , Link Pojos
	//Hotel Setup
	private String HotelName = "";
	private String Supplier = "";
	private String AddressLine1 = "";
	private String AddressLine2 = "";
	private String Country = "";
	private String City = "";
	private String State = "";
	private String ZipCode = "";
	private String Location = "";
	private String StarCategory = "";
	private boolean DisplayinCC = true;
	private boolean DisplayinWeb = true;
	private boolean DisplayinXML = true;
	private String DisplayRank = "";
	private boolean isHotelActive = true;
	private String SuspendHotelAvailbility = "";
	private String CustomerAlertWhenBookingIsMade = "";
	private String HotelRateType = "";
	private String NetSuiteProviderID = "";
	private String NetSuiteVendorID = "";
	private String BonotelPropertyID = "";
	private boolean EZYeildUpdateEnable = false;
	private String 	Market = "";
	private boolean SendCCLink = false;
	
	//Contract
	private ArrayList<HotelContract> Contracts = new ArrayList<HotelContract>();
	private String InventoryObtainedBy = "";
	private boolean AllowtoExchangeAllotmentsBetweenRoomTypes=false;
	//Policies
	private String ChildrenAllowed="";
	private String 	ChargesFreeAgeGroupFrom="";
	private String ChildRateApplicableAgeGroupFrom="";
	private String CheckinTime="";
	private String CheckOutTime="";
	private String 	HotelWeekdayClassificationFrom="";
	private String HotelWeekendClassificationFrom="";
	//Payment Policies 
	private boolean PrepaymentRequired=true;
	private boolean CreditCardProcessingEnabled=true;
	private String AcceptableCreditCardTypes="";
	
	private ArrayList<HotelPolicies> Policies =new ArrayList<HotelPolicies>();
	private ArrayList<Taxes> taxes=new ArrayList<Taxes>();
	private ArrayList<HotelFee> hotelfee=new ArrayList<HotelFee>();
	private ArrayList<ResortFee> resortfee=new ArrayList<ResortFee>();
	private ArrayList<OccupancyTax> occupancyTax=new ArrayList<OccupancyTax>();
	
	//profitMarkup
	private ArrayList<ProfitMarkup> profitMarkup=new ArrayList<ProfitMarkup>();
	
	private ArrayList<Room> rooms = new ArrayList<Room>();
	private ArrayList<RatePlan> rateplan= new ArrayList<RatePlan>();
	private ArrayList<ContactDetails> Contacts = new ArrayList<ContactDetails>();
	
	
	
	
}
