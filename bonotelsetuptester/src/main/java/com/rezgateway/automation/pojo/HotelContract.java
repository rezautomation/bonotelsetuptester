package com.rezgateway.automation.pojo;

import com.rezgateway.automation.enums.ContractType;

public class HotelContract {


	private ContractType mainContractType = ContractType.Net_Rate;
	private ContractType whenNoInventoryContractType = ContractType.None;
	private boolean Presentage=false;
	private boolean AbsoluteValue=false;
	private String 	Amount  =  "";
	private String from="";
	private String to="";

	public ContractType getMainContractType() {
		return mainContractType;
	}
	public void setMainContractType(ContractType mainContractType) {
		this.mainContractType = mainContractType;
	}
	public ContractType getWhenNoInventoryContractType() {
		return whenNoInventoryContractType;
	}
	public void setWhenNoInventoryContractType(
			ContractType whenNoInventoryContractType) {
		this.whenNoInventoryContractType = whenNoInventoryContractType;
	}
	public boolean isPresentage() {
		return Presentage;
	}
	public void setPresentage(boolean presentage) {
		Presentage = presentage;
	}
	public boolean isAbsoluteValue() {
		return AbsoluteValue;
	}
	public void setAbsoluteValue(boolean absoluteValue) {
		AbsoluteValue = absoluteValue;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	
	
}
