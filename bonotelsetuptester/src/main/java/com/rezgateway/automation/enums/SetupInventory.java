package com.rezgateway.automation.enums;

public enum SetupInventory {
	TotalRooms,MinimumNightsStay, MaximumNightsStay,Cutoff;
	public static SetupInventory getSearchByType(String type) {
		if (type.trim().equalsIgnoreCase("TotalRooms")) {
			return SetupInventory.TotalRooms;	
			
		} else if (type.trim().equalsIgnoreCase("MinimumNightsStay")) {
			return SetupInventory.MinimumNightsStay;
		} else if (type.trim().equalsIgnoreCase("MaximumNightsStay")) {
			return SetupInventory.MaximumNightsStay;
		} else if (type.trim().equalsIgnoreCase("Cutoff")) {
			return SetupInventory.Cutoff;
		}
		return null;
	}
}
