package com.rezgateway.automation.enums;

public enum ContractType {

	Net_Rate, Commissionable_Rate, Free_sell, Request, None;

	public static ContractType getSearchByType(String type) {
		if (type.trim().equalsIgnoreCase("NET")) {
			return ContractType.Net_Rate;	
		} else if (type.trim().equalsIgnoreCase("COM")) {
			return ContractType.Commissionable_Rate;
		} else if (type.trim().equalsIgnoreCase("FREE")) {
			return ContractType.Free_sell;
		} else if (type.trim().equalsIgnoreCase("Req")) {
			return ContractType.Request;
		}else {
			return ContractType.None;
		}
	}

}
