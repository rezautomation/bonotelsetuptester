package com.rezgateway.automation.enums;

public enum CancellationBasedOnType {

	Value,Reservation_Value, No_Night;
	public static CancellationBasedOnType getSearchByType(String type){
		
		if (type.trim().equalsIgnoreCase("Value")) {
			return CancellationBasedOnType.Value;
		} else if (type.trim().equalsIgnoreCase("Reservation_Value")) {
			return CancellationBasedOnType.Reservation_Value;
		} else if(type.trim().equalsIgnoreCase("No_Night")){
			return CancellationBasedOnType.No_Night;
		}
		return null;
		
	}   
	
	}

